# SimpleModal OSX Style Modal Dialog Dynamic Photo Grid

### OSX Style Modal Dialog

A modal dialog configured to behave like an OSX dialog. Demonstrates the use of the callbacks as well as custom styling and a handful of options

Inspired by [ModalBox](http://okonet.ru/projects/modalbox/) and OSX style dialog built with [Prototype](http://www.prototypejs.org/), Dynamic Photo Grid [Syknapse](https://github.com/Syknapse/Dynamic-Photo-Grid) & [Elena Moral](https://github.com/elena-in-code/Dynamic-Photo-Grid) built with [HTML5](https://github.com/topics/html5built)

* SimpleModal is a lightweight jQuery Plugin which provides a powerful interface for modal dialog development. Think of it as a modal dialog framework.

* SimpleModal gives you the flexibility to build whatever you can envision, while shielding you from related cross-browser issues inherent with UI development.

* As you can see by this example, SimpleModal can be easily configured to behave like an OSX dialog. With a handful options, 2 custom callbacks and some styling, you have a visually appealing dialog that is ready to use!

![screenshot](assets/screenshot.png)

### Optional:
You can replace below part with youutube or another video as you like
~~~
  <video width='400' controls autoplay loop muted>
  <source src='assets/mov_bb.mp4' type='video/mp4'>
  <source src='assets/mov_bb.ogg' type='video/ogg'>
  Your browser does not support HTML5 video.
  </video>
~~~
or
~~~
<!-- modal content -->
		<div id='osx-modal-content'>
			<div id='osx-modal-title'>OSX Style Modal Dialog</div>
			<div class='close'><a href='#' class='simplemodal-close'>x</a></div>
			<div id='osx-modal-data'>
<!-- **********LayerOne*********** -->
---
---
---
<!-- **********End of Layer*********** -->	
~~~
~~~
Example:
<iframe width="560" height="315" src="https://www.youtube.com/embed/e2icQFxhp3w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
~~~

[DEMO](https://readloud.github.io/SimpleModal-Dynamic-Photo-Grid/)
